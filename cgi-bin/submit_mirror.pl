#!/usr/bin/perl -wT

# Copyright (C) 1998 James Treacy
# Copyright (C) 2007 Josip Rodin

# used by www.d.o/mirror/submit

use HTML::Entities;

# encode html entities appropriately; if given an array in list
# context, return the array; otherwise return the concatenation of
# everything given
sub html_escape {
    my @r = map {HTML::Entities::encode_entities($_)} @_;
    if (wantarray) {
        return @r;
    } else {
        return join('',@r);
    }
}

require 5.001;

my $notsubmitted = "<p><strong>Entry not submitted!</strong>";

my $public_dest = 'submit@bugs.debian.org';
my $private_dest = 'mirrors@debian.org';
my $ENVELOPE_FROM = 'mirrors-submit_mirror@debian.org';

# obligatory to avoid -T crashing and burning -joy, 2007-01-29
$ENV{PATH} = "/bin:/usr/bin";

my $logfile = "/srv/cgi.debian.org/log/submit_mirror.log";
open LOG, ">>$logfile";

# $debug=0;
use CGI;
$query = new CGI;
print $query->header;
print $query->start_html(-title=>'Debian mirror submission');

my $submissiontype = $query->param('submissiontype');

my $site = $query->param('site');
if (!defined($site) or $site eq '') {
   print "<p>No site given.\n";
   print $notsubmitted;
   exit;
}
$site =~ s,(ht|f)tp://(.+?),,;
$site =~ s,/$,,;
if ($site =~ /^([\w.-]+)$/) {
  $site = $1; # now untainted
} else {
  print "<p>Broken data given as site name: ".html_escape($query->param('site'))."\n";
  print $notsubmitted;
  exit;
}
my $aliases = $query->param('aliases');

my %mirror_types = (
	'archive-http' => '',
	'archive-rsync' => '',
	'cdimage-http' => '',
	'cdimage-rsync' => '',
	'old-http' => '',
	'old-rsync' => '',
);
foreach $type (keys %mirror_types) {
        if (defined($query->param($type)) && $query->param($type) ne '') {
          if ($query->param($type) =~ /^[\/\w-]+$/) {
            $mirror_types{$type} = $query->param($type);
          } else {
            print "Broken data given: ".html_escape($query->param($type))."\n";
            print $notsubmitted;
            exit;
          }
        }
	if (defined $mirror_types{$type} && $mirror_types{$type} ne '') {
		$mirror_types{$type} =~ s,(http|ftp|rsync)://(.+?)/,,;
		$mirror_types{$type} =~ s,(.*):,,;
		$mirror_types{$type} =~ s,/?$,/,;
		if ($type =~ /rsync/) {
			$mirror_types{$type} =~ s,^/,,;
	   	} else {
			$mirror_types{$type} =~ s,^/?,/,;
   		}
	}
}
my %tracedir = (), %tracefile = ();
for my $i ('archive-http', 'archive-rsync') {
  $tracedir{$i} = "project/trace/";
  $tracefile{$i} = "ftp-master.debian.org";
}
for my $i ('cdimage-http', 'cdimage-rsync') {
  $tracedir{$i} = "project/trace/";
  $tracefile{$i} = "cdimage.debian.org";
}
for my $i ('old-http', 'old-rsync') {
  $tracedir{$i} = "project/trace/";
  $tracefile{$i} = "archive.debian.org";
}
@architectures = $query->param('architectures');
my $maint_name = $query->param('maint_name');
my $maint_public_email = $query->param('maint_public_email');
$maint_public_email =~ s,^<,,; $maint_public_email =~ s,>$,,;
my $country = $query->param('country');
my $location = $query->param('location');
my $sponsor_name = $query->param('sponsor_name');
my $sponsor_url = $query->param('sponsor_url');
my $comment = $query->param('comment');
if (defined $comment) {
	$comment =~ s/\n/\n /gs;
	$comment =~ s/\r//gs;
}

# print $query->dump;
# print "<hr>\n";

print "The following information was collected:\n";

my $msg;
my $extramsg = '';
if (defined($submissiontype) && $submissiontype =~ /^(new|update)$/) {
   $msg .= "Submission-Type: $submissiontype\n";
   print "<p>Submission-Type: ".html_escape($submissiontype)."</p>\n";
} else {
   print "<p>Submission type not given.\n";
   print $notsubmitted;
   exit;
}
$msg .= "Site: $site\n";
print "<p>Site: $site<br>\n";
(my $s_name, my $s_aliases, my $s_addrtype, my $s_length, my @s_addrs) = gethostbyname $site;
if ($#s_addrs == -1) {
  print LOG time, " site $site unresolvable\n";
  print "<p>Unable to resolve the site name in DNS.\n";
#  print scalar(@s_addrs);
  print $notsubmitted;
  exit;
}
if (defined($aliases) && $aliases =~ /[\/\w]/) {
   @aliases = split(',', $aliases);
   foreach (@aliases) {
      s/ //g;
      $msg .= "Aliases: $_\n";
      print "Aliases: $_<br>\n";
   }
}
if (scalar(@architectures)) {
  my $al;
  foreach my $a (@architectures) {
    if ($a =~ /^([\w-]+)$/) {
      $a = $1; # now untainted
      $al .= $a." ";
    }
  }
  $al =~ s/ *$//;
  $msg .= "Archive-architecture: ". $al ."\n";
  print "Archive-architecture: ". $al ."<br>\n";
} else {
  print "<p>You have to be mirroring some architectures, please check the boxes.\n";
  print $notsubmitted;
  exit;
}
foreach my $mtype (sort keys %mirror_types) {
  if ($mirror_types{$mtype} ne '') {
    if ($mirror_types{$mtype} =~ /[\/\w]/) {
      $mtypeu = ucfirst $mtype;
      $mtypeu =~ s/^Cdim/CDIm/;
      $msg .= "$mtypeu: $mirror_types{$mtype}\n";
      print "$mtypeu: $mirror_types{$mtype}<br>\n";
      if ($mtype =~ /-http$/) {
        my $tracebaseurl = "http://".$site.$mirror_types{$mtype}.$tracedir{$mtype};
        my $trace     = $tracebaseurl . $tracefile{$mtype};
        my $sitetrace = $tracebaseurl . ${site};
        $extramsg .= "\n";
        $extramsg .= "Trace Url: $tracebaseurl\n";
        $extramsg .= "Trace Url: $trace\n";
        $extramsg .= "Trace Url: $sitetrace\n";
        use LWP::UserAgent;
        my $ua = LWP::UserAgent->new; $ua->timeout(10);
        my $myurl = $ua->get($trace);
        my $myurl_site = $ua->get($sitetrace);
        if ($myurl->is_success && $myurl_site->is_success) {
#          print LOG time, " site $site $mirror_types{$mtype} HTTP fine... " . $myurl->content . "\n";
        } else {
          print LOG time, " site $site $mirror_types{$mtype} HTTP bad " . $myurl->status_line . "\n";
          print "<p>A proper project/trace directory was not found on the HTTP server (under $mirror_types{$mtype}).\n";
          print "<p>The trace directory needs to be fully synced from upstream, and need a local tracefile named after the site name.";
          print " <br>Fetching ".html_escape($trace)." we received: ".html_escape($myurl->status_line).".";
          print " <br>Fetching ".html_escape($sitetrace)." we received: ".html_escape($myurl_site->status_line).".";
          print '<p>Please have a look at <a href="http://www.debian.org/mirror/ftpmirror">the documentation</a>.';
          print "<p>If you don't understand this error message, please contact us.";
          print $notsubmitted;
          exit;
        }
      }
    } else {
      print "<p>Bad data in $mtype: " . $mirror_types{$mtype} ."<br>\n";
    }
  }
}
if (
    (!defined($mirror_types{'archive-http'}) || $mirror_types{'archive-http'} eq '') &&
    (!defined($mirror_types{'cdimage-http'}) || $mirror_types{'cdimage-http'} eq '')
    ) {
  print "<p>Broken data given - you have to be carrying more than that...\n";
  print "<p>If you don't understand this error message, please contact us.";
  print $notsubmitted;
  exit;
}

if (defined($maint_name)  && $maint_name =~ /\w/ &&
    defined($maint_public_email) && $maint_public_email =~ /[\w.]+\@\S+\.\w+/) {
   $msg .= "Maintainer: $maint_name <".$maint_public_email.">\n";
   print "Maintainer: $maint_name &lt;".$maint_public_email."&gt;<br>\n";
} else {
   print "<p>Both maintainer name and public maintainer email are required.";
  print $notsubmitted;
   exit;
}
if (defined($country) && $country =~ /\w\w/) {
   $msg .= "Country: $country\n";
   print "Country: $country<br>\n";
} else {
# submit page uses a droplist instead of a field, but for spambots
  print "<p><em>Country</em> is required and you must use the two letter
  iso3166 abbreviation for the country name.";
  print $notsubmitted;
  exit;
}
if (defined($location) && $location =~ /\w/) {
   $msg .= "Location: $location\n";
   print "Location: $location<br>\n";
}
if (defined($sponsor_url) && ($sponsor_url ne '') && not($sponsor_url =~ /https?:\/\/.*/)) {
   print "<p>Sponsor URL format is http[s]://example.com/";
  print $notsubmitted;
   exit;
}
if (defined($sponsor_name) && $sponsor_name =~ /\w/ &&
    defined($sponsor_url) && $sponsor_url =~ /\w/) {
   $msg .= "Sponsor: $sponsor_name $sponsor_url\n";
   print "Sponsor: $sponsor_name $sponsor_url<br>\n";
}
if (defined($comment) && $comment =~ /\w/) {
   $msg .= "Comment: $comment\n";
   print "Comment: $comment<br>\n";
}

print "<p>If there was an error in your submission,\n";
print "please go back and re-submit an update, or mail $private_dest.</p>\n";

$msg .= "\n\n\n".$extramsg;

# added LOGDATA to be able to debug better, it seems that the script/SMTP
# still eats some submissions :( -joy, 2007-10-26
open LOGDATA, ">>/srv/cgi.debian.org/log/submit_mirror.data";

print LOG time, " site $site: $maint_name, $maint_public_email mailing BTS\n";
print LOGDATA "running mail($maint_name, $maint_public_email, $public_dest, debbugs, $submissiontype, $site, <msg>)\n";
print LOGDATA "with last argument being:\n$msg\n";
mail($maint_name, $maint_public_email, $public_dest, "debbugs", $submissiontype, $site, $msg);

print LOG time, " site $site: $maint_name, $maint_public_email mailing m\@d.o\n";
print LOGDATA "running mail($maint_name, $maint_public_email, $private_dest, email, $submissiontype, $site, <msg>)\n";
print LOGDATA "with last argument being:\n$msg\n";
mail($maint_name, $maint_public_email, $private_dest, "email", $submissiontype, $site, $msg);

print $query->end_html;

close LOG;
close LOGDATA;

sub mail {
  my ($maint_name, $maint_email, $to, $desttype, $submissiontype, $site, $body) = @_;
  my ($text, @command, $pid);
  if ($maint_name =~ /^([\w .,-\@\/]+)$/) {
    $maint_name = $1; # now untainted
  } else {
    print "Bad data in maintainer name: $maint_name";
    exit;
  }
  if ($maint_email =~ /^([\w.+-]+\@[\w.-]+)$/) {
    $maint_email = $1; # now untainted
  } else {
    print "Bad data in maintainer e-mail: $maint_email";
    exit;
  }
  my $from = "\"$maint_name\" <$maint_email>";

  $text = "From: $from\n"
         ."To: $to\n"
         ."X-Sender: unauthenticated web user of submit_mirror.pl, by $to\n";
  if ($desttype eq "debbugs") {
    if ($submissiontype eq "new") {
      $text .= "Subject: mirror submission for $site\n";
    } elsif ($submissiontype eq "update") {
      $text .= "Subject: mirror listing update for $site\n";
    }
  } else {
    if ($submissiontype eq "new") {
      $text .= "Subject: New Debian mirror submission ($site)\n";
    } elsif ($submissiontype eq "update") {
      $text .= "Subject: Updated Debian mirror submission ($site)\n";
    }
  }
  $text .= "\n";
  if ($desttype eq "debbugs") {
    my $severity = $submissiontype eq "new" ? 'wishlist' : 'minor';
    my $tag      = $submissiontype eq "new" ? 'mirror-submission' : 'mirror-list';

    $text .= "Package: mirrors\n";
    $text .= "Severity: $severity\n";
    $text .= "User: mirrors\@packages.debian.org\n";
    $text .= "Usertags: $tag\n";
    $text .= "\n";
  }
  $text .= $body;

  print "<p>Recording...\n";

  @command = ("/usr/sbin/sendmail", "-f", $ENVELOPE_FROM, $to);
  my $sleep_count = 0;
  do {
    $pid = open(SENDMAIL_CHILD, "|-");
    unless (defined $pid) {
      warn "cannot fork: $!";
      print LOG time, " site $site mail problem, cannot fork: $!\n";
      die "bailing out" if $sleep_count++ > 6;
      sleep 10;
    }
  } until defined $pid;

  if ($pid) { # in parent
    print SENDMAIL_CHILD $text;
    unless(close(SENDMAIL_CHILD)) {
      warn "child process sending mail exited $?";
      print LOG time, " site $site mail problem, child exited $?\n";
      print "failed, please notify $private_dest.\n";
    }
    print "done.\n";
  } else { # in child
    exec @command;
  }
# print "sending mail currently disabled\n";
# print $text;
}
