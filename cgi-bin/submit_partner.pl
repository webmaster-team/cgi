#!/usr/bin/perl -wT

# Copyright (C) 1998 James Treacy
# Copyright (C) 2007 Josip Rodin

# used by www.d.o/partners/partners-form

require 5.001;

my $notsubmitted = "<p><strong>Entry not submitted!</strong>";

my $public_dest = 'debian-www@lists.debian.org';
my $private_dest = 'partners@debian.org';

# By default, DSA-administered machines do not trust any CA certs 
# DSA has provided /etc/ssl/ca-global as a workaround.
# See https://wiki.debian.org/ServicesSSL

my $ca_dir = '/etc/ssl/ca-global';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

# obligatory to avoid -T crashing and burning -joy, 2007-01-29
$ENV{PATH} = "/bin:/usr/bin";

my $logfile = "/srv/cgi.debian.org/log/submit_partner.log";
open LOG, ">>$logfile";

# $debug=0;
use CGI;
$query = new CGI;
print $query->header;
print $query->start_html(-title=>'New Debian Partners Proposal - sent by Debian Partners web page');

my $partner = $query->param('partner');
if (!defined($partner) or $partner eq '') {
   print "<p>No company name given.\n";
   print $notsubmitted;
   exit;
}
if ($partner =~ /^([\w\s.-]+)$/) {
  $partner = $1; # now untainted
} else {
  print "<p>Broken data given as company name: ".$query->param('partner')."\n";
  print $notsubmitted;
  exit;
}

my $url = $query->param('url');
my $email = $query->param('email');
my $contact_person = $query->param('contact_person');
my $description = $query->param('description');
if (defined $description) {
	$description =~ s/\n/\n /gs;
	$description =~ s/\r//gs;
}

# print $query->dump;
# print "<hr>\n";

print "The following information was collected:\n";

my $msg;

$msg .= "Company name=\"$partner\"\n";
print "<p>Company name: $partner<br>\n";


if (!defined($url) or $url eq '') {
   print "<p>No URL given.\n";
   print $notsubmitted;
   exit;
}
if ($url =~ /^([\w.:\/~-]+)$/) {
  $url = $1; # now untainted
  $msg .= "\turl=\"$url\"\n";
  print "URL: $url<br>\n";
} else {
  print "<p>Broken data given as company URL: ".$query->param('url')."\n";
  print $notsubmitted;
  exit;
}
my $site = $url;
$site =~ s,^\s*\w+://,,;
$site =~ s,^([^/:]+).*$,$1,;
(my $s_name, my $s_aliases, my $s_addrtype, my $s_length, my @s_addrs) = gethostbyname $site;
if ($#s_addrs == -1) {
  print LOG time, " site $site unresolvable\n";
  print "<p>Unable to resolve the site name ($site) in DNS.\n";
#  print scalar(@s_addrs);
   print $notsubmitted;
  exit;
}

use LWP::UserAgent;
my $ua = LWP::UserAgent->new; $ua->timeout(10);
my $urltoget = $url;
my $myurl = $ua->get($urltoget);
if ($myurl->is_success) {
#          print LOG time, " site $site $url HTTP fine... " . $myurl->content . "\n";
} else {
  print LOG time, " site $site $url HTTP bad " . $myurl->status_line . "\n";
  print "<p>The specified web site ($url) cannot be accessed.\n";
  print "<p>The error message returned was: " . $myurl->status_line . "\n";
  print "<p>If you don't understand this error message, please contact us.";
  print $notsubmitted;
  exit;
}

#print "<pre>email: $email</pre>";
if (!defined($email) or $email eq '') {
   print "<p>No email address given.\n";
   print $notsubmitted;
   exit;
}
if ($email =~ /^<?([\w.]+\@\S+\.\w+)>?$/) {
  $email = $1; # now untainted
} else {
  print "<p>Broken data given as the email address: ".$email."\n";
  print $notsubmitted;
  exit;
}

$msg .= "\tcontact_mail=\"mailto:".$email."\"\n";
print "E-mail: &lt;".$email."&gt;<br>\n";

if (!defined($contact_person) or $contact_person eq '') {
   print "<p>No contact person given.\n";
   print $notsubmitted;
   exit;
}

$msg .= "\tcontact_person=\"$contact_person\"\n";
print "Contact person: $contact_person<br>\n";


if (defined($description) && $description =~ /\w/) {
   $msg .= "Description: $description\n";
   print "Description: $description<br>\n";
}

print "<p>If there was an error in your submission,\n";
print "please go back and re-submit an update, or mail $private_dest.</p>\n";

# added LOGDATA to be able to debug better, it seems that the script/SMTP
# still eats some submissions - lines copied from submit_mirror.pl
open LOGDATA, ">>/srv/cgi.debian.org/log/submit_partner.data";

print LOG time, " site $site: $company, $email mailing partners\@d.o\n";
print LOGDATA "running mail(submit_partner.pl, $email, $private_dest, email, $partner, <msg>)\n";
print LOGDATA "with last argument being:\n$msg\n";
mail("submit_partner.pl", $email, $private_dest, "email", $partner, $msg);

print $query->end_html;

close LOG;
close LOGDATA;

sub mail {                 
  my ($name, $email, $to, $desttype, $site, $body) = @_;
  my ($text, @command, $pid);
  if ($name =~ /^([\w .,-\@\/]+)$/) {
    $name = $1; # now untainted
  } else {
    print "Bad data in the name: $name";
    exit;
  }
  if ($email =~ /^([\w.+-]+\@[\w.-]+)$/) {
    $email = $1; # now untainted
  } else {
    print "Bad data in the e-mail: $email";
    exit;
  }
# overridden header sender to avoid occasional badness...
#  my $from = "\"$name\" <www-data>";
# but using it in reply-to -joy, 2007-05-14
#  my $replyto = "\"$name\" <$email>";

  my $from = "\"$name\" <$email>";

  $text = "From: $from\n"
         ."To: $to\n"
#         ."Reply-To: $replyto\n"
         ."X-Sender: unauthenticated web user of submit_partner.pl, by $to\n";
  $text .= "Subject: New Debian Partners Proposal - $site\n";
  $text .= "\n";
  $text .= $body;

  print "<p>Recording...\n";

  @command = ("/usr/sbin/sendmail", "-f", $from, $to);
  my $sleep_count = 0;
  do {
    $pid = open(SENDMAIL_CHILD, "|-");
    unless (defined $pid) {
      warn "cannot fork: $!";
      print LOG time, " site $site mail problem, cannot fork: $!\n";
      die "bailing out" if $sleep_count++ > 6;
      sleep 10;
    }
  } until defined $pid;

  if ($pid) { # in parent
    print SENDMAIL_CHILD $text;
    unless(close(SENDMAIL_CHILD)) {
      warn "child process sending mail exited $?";
      print LOG time, " site $site mail problem, child exited $?\n";
      print "failed, please notify $private_dest.\n";
    }
    print "done.\n";
  } else { # in child
    exec @command;
  }
# print "sending mail currently disabled\n";
# print $text;
}
