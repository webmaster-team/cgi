#!/usr/bin/perl -wT

# Copyright (C) 1998 James Treacy
# Copyright (C) 2007 Josip Rodin

# used by www.d.o/CD/vendors/adding-form

require 5.001;

my $notsubmitted = "<p><strong>Entry not submitted!</strong>";

my $public_dest = 'debian-www@lists.debian.org';
my $private_dest = 'cdvendors@debian.org';

# By default, DSA-administered machines do not trust any CA certs 
# DSA has provided /etc/ssl/ca-global as a workaround.
# See https://wiki.debian.org/ServicesSSL

my $ca_dir = '/etc/ssl/ca-global';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

# obligatory to avoid -T crashing and burning -joy, 2007-01-29
$ENV{PATH} = "/bin:/usr/bin";

my $logfile = "/srv/cgi.debian.org/log/submit_cdvendor.log";
open LOG, ">>$logfile";

# $debug=0;
use CGI;
$query = new CGI;
print $query->header;
print $query->start_html(-title=>'Debian CD vendor submission');

my $vendor = $query->param('vendor');
if (!defined($vendor) or $vendor eq '') {
   print "<p>No vendor name given.\n";
   print $notsubmitted;
   exit;
}
if ($vendor =~ /^([\w\s.-]+)$/) {
  $vendor = $1; # now untainted
} else {
  print "<p>Broken data given as vendor name: ".$query->param('vendor')."\n";
  print $notsubmitted;
  exit;
}
my $submissiontype = $query->param('submissiontype');

my $url = $query->param('url');
my $urldebcd = $query->param('urldebcd');
my $donates = $query->param('donates');
my $email = $query->param('email');
my $country = $query->param('country');
my $offerscd = $query->param('offerscd');
my $offersdvd = $query->param('offersdvd');
my $offersbd = $query->param('offersbd');
my $offersusb = $query->param('offersusb');
my $shipping = $query->param('shipping');
my @architectures = $query->param('architectures');
my $comment = $query->param('comment');
if (defined $comment) {
	$comment =~ s/\n/\n /gs;
	$comment =~ s/\r//gs;
}

# print $query->dump;
# print "<hr>\n";

print "The following information was collected:\n";

my $msg;
if (defined($submissiontype) && $submissiontype =~ /^(new|update)$/) {
   $msg .= "Submission-Type: $submissiontype\n\n";
   print "<p>Submission-Type: $submissiontype<br>\n";
} else {
   print "<p>Submission type not given.\n";
   print $notsubmitted;
   exit;
}

$msg .= "Vendor name=\"$vendor\"\n";
print "<p>Vendor: $vendor<br>\n";


if (!defined($url) or $url eq '') {
   print "<p>No URL given.\n";
   print $notsubmitted;
   exit;
}
if ($url =~ /^([\w.:\/~-]+)$/) {
  $url = $1; # now untainted
  $msg .= "\turl=\"$url\"\n";
  print "URL: $url<br>\n";
} else {
  print "<p>Broken data given as vendor URL: ".$query->param('url')."\n";
  print $notsubmitted;
  exit;
}
my $site = $url;
$site =~ s,^\s*\w+://,,;
$site =~ s,^([^/:]+).*$,$1,;
(my $s_name, my $s_aliases, my $s_addrtype, my $s_length, my @s_addrs) = gethostbyname $site;
if ($#s_addrs == -1) {
  print LOG time, " site $site unresolvable\n";
  print "<p>Unable to resolve the site name ($site) in DNS.\n";
#  print scalar(@s_addrs);
   print $notsubmitted;
  exit;
}

use LWP::UserAgent;
my $ua = LWP::UserAgent->new; $ua->timeout(10);
my $urltoget = $url;
my $myurl = $ua->get($urltoget);
if ($myurl->is_success) {
#          print LOG time, " site $site $url HTTP fine... " . $myurl->content . "\n";
} else {
  print LOG time, " site $site $url HTTP bad " . $myurl->status_line . "\n";
  print "<p>The specified web site ($url) cannot be accessed.\n";
  print "<p>The error message returned was: " . $myurl->status_line . "\n";
  print "<p>If you don't understand this error message, please contact us.";
  print $notsubmitted;
  exit;
}

if (!defined($urldebcd) or $urldebcd eq '') {
   print "<p>No URL for the Debian info page given.\n";
   print $notsubmitted;
   exit;
}
if ($urldebcd =~ /^([\w.:\/~?-]+)$/) {
  $urldebcd = $1; # now untainted
  $msg .= "\tdeburl=\"$urldebcd\"\n";
  print "URL-Debian: $urldebcd<br>\n";
} else {
  print "<p>Broken data given as vendor's Debian-related URL: ".$query->param('urldebcd')."\n";
  print $notsubmitted;
  exit;
}
my $debcdsite = $urldebcd;
$debcdsite =~ s,^\s*\w+://,,;
$debcdsite =~ s,^([^/:]+).*$,$1,;
$s_name = undef; $s_aliases = undef; $s_addrtype = undef; $s_length = undef; @s_addrs = undef;
($s_name, $s_aliases, $s_addrtype, $s_length, @s_addrs) = gethostbyname $debcdsite;
if ($#s_addrs == -1) {
  print LOG time, " site $debcdsite unresolvable\n";
  print "<p>Unable to resolve the info web page site name ($debcdsite) in DNS.\n";
#  print scalar(@s_addrs);
  print $notsubmitted;
  exit;
}

$urltoget = $urldebcd;
$myurl = $ua->get($urltoget);
if ($myurl->is_success) {
#          print LOG time, " site $site $urldebcd HTTP fine... " . $myurl->content . "\n";
} else {
  print LOG time, " site $site $urldebcd HTTP bad " . $myurl->status_line . "\n";
  print "<p>The specified web site ($urldebcd) cannot be accessed.\n";
  print "<p>The error message returned was: " . $myurl->status_line . "\n";
  print "<p>If you don't understand this error message, please contact us.";
  print $notsubmitted;
  exit;
}

if ($myurl->content !~ /Debian/) {
  print LOG time, " site $site $urldebcd HTTP bad - non-Debian\n";
  print "<p>The specified web site ($urldebcd) doesn't mention Debian.\n";
  print "<p>If you don't understand this error message, please contact us.";
  print $notsubmitted;
  exit;
}

#  print "<p>Broken data given - you have to be carrying more than that...\n";
#  print "<p>If you don't understand this error message, please contact us.";
#  print $notsubmitted;
#  exit;

#print "<pre>email: $email</pre>";
if (!defined($email) or $email eq '') {
   print "<p>No email address given.\n";
   print $notsubmitted;
   exit;
}
if ($email =~ /^<?([\w.]+\@\S+\.\w+)>?$/) {
  $email = $1; # now untainted
} else {
  print "<p>Broken data given as the email address: ".$email."\n";
  print $notsubmitted;
  exit;
}

$msg .= "\tcontacturl=\"mailto:".$email."\"\n";
print "E-mail: &lt;".$email."&gt;<br>\n";

if (!defined($country) or $country eq '') {
   print "<p>No country given.\n";
   print $notsubmitted;
   exit;
}
if ($country =~ /^(\w\w\s\w+.+)$/) {
  $country = $1; # now untainted
} else {
# submit page uses a droplist instead of a field, but for spambots
  print "<p><em>Country</em> is required and you must use the two letter
  iso3166 abbreviation for the country name.";
  print $notsubmitted;
  exit;
}

$country =~ s/GB Great Britain \(UK\)/GB Great Britain/;
$msg .= "\tcountry=\"$country\"\n";
print "Country: $country<br>\n";

if (!defined($donates) or $donates eq '') {
  $donates = "no"
}
if ($donates =~ /^(\w+)$/) {
  $donates = $1; # now untainted
  $msg .= "\tcontribution=\"$donates\"\n";
  print "Donates: $donates<br>\n";
} else {
  print "<p>Broken data given as the donation flag: ".$donates."\n";
  print $notsubmitted;
  exit;
}

if (!defined($shipping) or $shipping eq '') {
   print "<p>No shipping flag given.\n";
   print $notsubmitted;
   exit;
}
if ($shipping =~ /^(\w+)$/) {
  $shipping = $1; # now untainted
  $msg .= "\tship=\"$shipping\"\n";
  print "Shipping-overseas: $shipping<br>\n";
} else {
  print "<p>Broken data given as the shipping flag: ".$shipping."\n";
  print $notsubmitted;
  exit;
}

if (!defined($offerscd) and !defined($offersdvd) and !defined($offersbd) and !defined($offersusb)) {
   print "<p>No flag about which kind of media is offered given.\n";
   print $notsubmitted;
   exit;
}

if (defined($offerscd)) {
  if ($offerscd =~ /^(\w+)$/) {
    $offerscd = $1; # now untainted
    $msg .= "\tcd=\"$offerscd\"\n";
    print "Offers-CDs: $offerscd<br>\n";
  } else {
    print "<p>Broken data given as the offerscd flag: ".$offerscd."\n";
    print $notsubmitted;
    exit;
  }
}

if (defined($offersdvd)) {
  if ($offersdvd =~ /^(\w+)$/) {
    $offersdvd = $1; # now untainted
    $msg .= "\tdvd=\"$offersdvd\"\n";
    print "Offers-DVDs: $offersdvd<br>\n";
  } else {
    print "<p>Broken data given as the offersdvd flag: ".$offersdvd."\n";
    print $notsubmitted;
    exit;
  }
}

if (defined($offersbd)) {
  if ($offersbd=~ /^(\w+)$/) {
    $offersbd = $1; # now untainted
    $msg .= "\tbd=\"$offersbd\"\n";
    print "Offers-BDs: $offersbd<br>\n";
  } else {
    print "<p>Broken data given as the offersbd flag: ".$offersbd."\n";
    print $notsubmitted;
    exit;
  }
}

if (defined($offersusb)) {
  if ($offersusb =~ /^(\w+)$/) {
    $offersusb = $1; # now untainted
    $msg .= "\tusb=\"$offersusb\"\n";
    print "Offers-USBs: $offersusb<br>\n";
  } else {
    print "<p>Broken data given as the offersusb flag: ".$offersusb."\n";
    print $notsubmitted;
    exit;
  }
}

if (@architectures) {
  my $al;
  foreach my $a (@architectures) {
    if ($a =~ /^([\w-]+)$/) {
      $a = $1; # now untainted
      $al .= $a." ";
    }
  }
  $msg .= "\tarchitectures=\"". $al ."\"\n";
  print "CD-architecture: ". $al ."<br>\n";
} else {
  print "<p>You have to be providing some architectures, please check the boxes.\n";
  print $notsubmitted;
  exit;
}

if (defined($comment) && $comment =~ /\w/) {
   $msg .= "Comment: $comment\n";
   print "Comment: $comment<br>\n";
}

print "<p>If there was an error in your submission,\n";
print "please go back and re-submit an update, or mail $private_dest.</p>\n";

# added LOGDATA to be able to debug better, it seems that the script/SMTP
# still eats some submissions - lines copied from submit_mirror.pl
open LOGDATA, ">>/srv/cgi.debian.org/log/submit_cdvendor.data";

print LOG time, " site $site: $vendor, $email mailing cdvendors\@d.o\n";
print LOGDATA "running mail(submit_cdvendor.pl, $email, $private_dest, email, $submissiontype, $vendor, <msg>)\n";
print LOGDATA "with last argument being:\n$msg\n";
mail("submit_cdvendor.pl", $email, $private_dest, "email", $submissiontype, $vendor, $msg);

print $query->end_html;

close LOG;
close LOGDATA;

sub mail {                 
  my ($name, $email, $to, $desttype, $submissiontype, $site, $body) = @_;
  my ($text, @command, $pid);
  if ($name =~ /^([\w .,-\@\/]+)$/) {
    $name = $1; # now untainted
  } else {
    print "Bad data in the name: $name";
    exit;
  }
  if ($email =~ /^([\w.+-]+\@[\w.-]+)$/) {
    $email = $1; # now untainted
  } else {
    print "Bad data in the e-mail: $email";
    exit;
  }
# overridden header sender to avoid occasional badness...
#  my $from = "\"$name\" <www-data>";
# but using it in reply-to -joy, 2007-05-14
#  my $replyto = "\"$name\" <$email>";

  my $from = "\"$name\" <$email>";

  $text = "From: $from\n"
         ."To: $to\n"
#         ."Reply-To: $replyto\n"
         ."X-Sender: unauthenticated web user of submit_cdvendor.pl, by $to\n";
  if ($desttype eq "debbugs") {
    if ($submissiontype eq "new") {
      $text .= "Subject: CD vendor submission for $site\n";
    } elsif ($submissiontype eq "update") {
      $text .= "Subject: CD vendor listing update for $site\n";
    }
} else {
    if ($submissiontype eq "new") {
      $text .= "Subject: New Debian CD vendor submission ($site)\n";
    } elsif ($submissiontype eq "update") {
      $text .= "Subject: Updated Debian CD vendor submission ($site)\n";
    }
  }
  $text .= "\n";
  if ($desttype eq "debbugs") {
    my $severity = $submissiontype eq "new" ? 'wishlist' : 'minor';
    my $tag      = $submissiontype eq "new" ? 'cdvendor-submission' : 'cdvendor-update';

    $text .= "Package: www.debian.org\n";
    $text .= "Severity: $severity\n";
    $text .= "User: www.debian.org\@packages.debian.org\n";
    $text .= "Usertags: $tag\n";
    $text .= "\n";
  }
  $text .= $body;

  print "<p>Recording...\n";

  @command = ("/usr/sbin/sendmail", "-f", $from, $to);
  my $sleep_count = 0;
  do {
    $pid = open(SENDMAIL_CHILD, "|-");
    unless (defined $pid) {
      warn "cannot fork: $!";
      print LOG time, " site $site mail problem, cannot fork: $!\n";
      die "bailing out" if $sleep_count++ > 6;
      sleep 10;
    }
  } until defined $pid;

  if ($pid) { # in parent
    print SENDMAIL_CHILD $text;
    unless(close(SENDMAIL_CHILD)) {
      warn "child process sending mail exited $?";
      print LOG time, " site $site mail problem, child exited $?\n";
      print "failed, please notify $private_dest.\n";
    }
    print "done.\n";
  } else { # in child
    exec @command;
  }
# print "sending mail currently disabled\n";
# print $text;
}
